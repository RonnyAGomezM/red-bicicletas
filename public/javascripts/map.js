var map = L.map('main_map').setView([-34.6184861,-58.3741231], 13);

L.tileLayer('https://api.mapbox.com/styles/v1/{id}/tiles/{z}/{x}/{y}?access_token={accessToken}', {
    attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, <a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
    maxZoom: 18,
    id: 'mapbox/streets-v11',
    tileSize: 512,
    zoomOffset: -1,
    accessToken: 'pk.eyJ1IjoicWFyaWx1Y2FzIiwiYSI6ImNrZm9nbTg0dzAybW4ycXFxeGtocWo4Nm0ifQ.X_55ljZ1iCcZ_zvbiNVrfw'
}).addTo(map);

$.ajax({
    dataType: "json",
    url:"api/bicicletas",
    success: function(result){
        console.log(result);
        result.bicicletas.forEach(function(bicicleta) {
            console.log(bicicleta._id);
            L.marker(bicicleta.ubicacion,{title:bicicleta._id}).addTo(map);
        });
    }
})