<<<<<<< HEAD
var mongoose = require('mongoose');
var Bicicleta = require('../../models/bicicleta');
var server = require('../../bin/www');
var request = require('request');

var baseURL = 'http://localhost:5000/api/bicicletas';

describe('Bicicleta API', () => {
    beforeAll((done) => {
        mongoose.connection.close().then(() => {
            var mongoDB = 'mongodb://localhost/testdb';
            mongoose.connect(mongoDB, { useNewUrlParser: true, useUnifiedTopology: true });
            mongoose.set('useCreateIndex', true);
    
            var db = mongoose.connection;
            db.on('error', console.error.bind(console, 'MongoDB connection error: '));
            db.once('open', function () {
                console.log('We are connected to test database!');
                done();
            });

        });

    });

    afterEach( (done) => {
        Bicicleta.deleteMany({}, function (err, success) {
            if (err) {
                console.log(err);
            }

            done();
        });
    });

    describe('GET Bicicletas /', () => {
        it('Status 200', (done) => {
            request.get(baseURL, function(err, resp, body){
                var result = JSON.parse(body);
                expect(resp.statusCode).toBe(200);
                expect(result.bicicletas.length).toBe(0);

                done();
            });
        });
    });

    describe('POST Bicicletas /create', () => {
        it('Status 200', (done) => {
            var headers = {'content-type': 'application/json'};
            var bici = '{ "code": 2, "color": "negro", "modelo": "montaña", "lat": 3.4760683, "lng": -76.4887271 }';

            request.post({
                headers: headers,
                url: `${ baseURL }/create`,
                body: bici
            }, function(err, resp, body) {
                expect(resp.statusCode).toBe(200);
                
                var oBici = JSON.parse(body).bicicleta;
                console.log(oBici);

                expect(oBici.color).toBe('negro');
                expect(oBici.modelo).toBe('montaña');
                expect(oBici.ubicacion[0]).toBe(3.4760683);
                expect(oBici.ubicacion[1]).toBe(-76.4887271);

                done();
            });

        });
    });

    describe('UPDATE Bicicletas /update', () => {
        it('Status 200', (done) => {
            var headers = {'content-type': 'application/json'};
            var bici = '{ "code": 3, "color": "azul", "modelo": "urbana", "lat": 3.4760683, "lng": -76.4887271 }';

            var a = new Bicicleta({
                code: 3,
                color: 'rojo',
                modelo: 'montaña',
                ubicacion: [3.4693968,-76.4887123]
            });
        
            Bicicleta.add(a, function (err, newBici) {
                request.put({
                    headers: headers,
                    url: `${ baseURL }/update`,
                    body: bici
                }, function(err, resp, body) {
                    expect(resp.statusCode).toBe(200);
                    
                    Bicicleta.findByCode(a.code, function (err, targetBici) {
                        console.log(targetBici);

                        expect(targetBici.color).toBe('azul');
                        expect(targetBici.modelo).toBe('urbana');
                        expect(targetBici.ubicacion[0]).toBe(3.4760683);
                        expect(targetBici.ubicacion[1]).toBe(-76.4887271);
                        
                        done();
                    });
                }); 
            });
        });
    });

    describe('DELETE Bicicletas /delete', () => {
        it('Status 204', (done) => {
            var a = Bicicleta.createInstance(4, 'gris', 'urbana', [3.4693968,-76.4887123]);
            
            Bicicleta.add(a, function (err, newBici) {
                var headers = {'content-type': 'application/json'};
                var bici = '{ "code": 4 }';
                
                request.delete({
                    headers: headers,
                    url: `${ baseURL }/delete`,
                    body: bici
                }, function(err, resp, body) {
                    expect(resp.statusCode).toBe(204);
                    
                    Bicicleta.allBicis(function (err, newBicis) {
                        expect(newBicis.length).toBe(0);

                        done();
                    });
                });
            });
        });
    });

});

/*var Bicicleta = require('../../models/bicicleta');
var request = require('request');
var server = require('../../bin/www');

describe('Bicicleta API',()=>{
    describe('GET BICICLETAS/',()=>{
        it('Status 200',()=>{
            expect(Bicicleta.allBicis.length).toBe(0);

            var aBici = new Bicicleta(1,'rojo','urbana',[-34.6184861,-58.3741231]);
            Bicicleta.add(aBici);

            request.get('http://localhost:5000/api/bicicletas',function(error,response,body){
                expect(response.statusCode).toBe(203)
            });
        });
    });

    describe('POST BICICLETAS/ create',()=>{
        it('Status 200',(done)=>{
            var headers={'content-type':'application/json'};
            var aBici={"id":10,"color":"Verdito","modelo":"carreras","lat":-34,"lng":-58};
            request.post({
                headers:headers,
                url:    'http://localhost:5000/api/bicicletas/create',
                body:   aBici
            }, function(error,response,body) {
                expect(response.statusCode).toBe(200);
                expect(Bicicleta.findById(10).color).toBe(aBici.color);
                done();
            });
        });
    });

});


*/
=======
var mongoose = require('mongoose')
var Bicicleta = require("../../models/bicicleta")
var server = require('../../bin/www')
var request = require('request')

var base_url = "http://localhost:5000/api/bicicletas"

describe('Bicicleta API', () => {

  // beforeEach((done) => {
  //   var mongoDB = 'mongodb://localhost/testdb'
  //   mongoose.connect(mongoDB, {
  //     useNewUrlParser: true,
  //     useUnifiedTopology: true
  //   })
  //   mongoose.set('useCreateIndex', true)

  //   const db = mongoose.connection;
  //   db.on('error', console.error.bind(console, 'Connection error'))
  //   db.once('open', () => {
  //     console.log("conectado a la BD")
  //     done()
  //   })
  // })

  beforeAll(function(done) {

    mongoose.connection.close().then(() => {

      var mongoDB = 'mongodb://localhost/testdb'
      mongoose.connect(mongoDB, { useNewUrlParser: true, useUnifiedTopology: true })
      mongoose.set('useCreateIndex', true)

      var db = mongoose.connection
      db.on('error', console.error.bind(console, 'MongoDB connection error: '))
      db.once('open', function () {
        console.log('We are connected to test database!')
        done()
      })
    })
  })

  afterEach((done) => {
    Bicicleta.deleteMany({}, (err, success) => {
      if (err) console.err(err)
      done()
    })
  })

  describe('GET BICICLETAS / ', () => {
    it('STATUS 200', (done) => {
      request.get(base_url, function (error, response, body) {
        var result = JSON.parse(body)
        expect(response.statusCode).toBe(200)
        expect(result.bicicletas.length).toBe(0)
        done()
      })
    })
  })

  describe('POST BICICLETAS /create', () => {
    it('STATUS 200', (done) => {
      var headers = {'Content-Type' : 'application/json'}
      var aBici = '{"code": 10, "color": "rojo", "modelo": "urbana", "lat": -27.451698, "lng": -58.990569}'
      request.post({
          headers: headers,
          url: base_url + '/create',
          body: aBici
        }, function(error, response, body) {
          expect(response.statusCode).toBe(200)
          let bici = JSON.parse(body).bicicleta
          expect(bici.code).toBe(10)
          expect(bici.color).toBe("rojo")
          expect(bici.modelo).toBe("urbana")
          expect(bici.ubicacion[0]).toBe(-27.451698)
          expect(bici.ubicacion[1]).toBe(-58.990569)
          done()
        })
    })
  })

  describe('DELETE BICICLETAS /delete', () => {
    it('STATUS 204', (done) => {

      //Se crea el registro a actualizar
      var a = Bicicleta.createInstance(1, "negro", "urbana", [-27.451698, -58.990569])
      Bicicleta.add(a, function(err, newBici) {
        var headers = {'Content-Type' : 'application/json'}
        var body = '{"code": 1}'
        request.delete({
          headers: headers,
          url: base_url + '/delete',
          body: body
        }, function(error, response, body) {
            expect(response.statusCode).toBe(204)
            expect(Bicicleta.allBicis.length).toBe(1)
            done()
        })
      })
    })
  })

  describe('PUT BICICLETAS /update', () => {
    it('STATUS 200', (done) => {
      var headers = {
        'Content-Type': 'application/json'
      }
      var a = Bicicleta.createInstance(2, "negro", "urbana", [-27.451698, -58.990569])
      Bicicleta.add(a, function(err, newBici) {
        var headers = {'Content-Type' : 'application/json'}
        var aBici = '{"code": 2, "color": "rojo", "modelo": "urbana", "lat": -27, "lng": -58}'
        request.put({
          headers: headers,
          url: base_url + '/update',
          body: aBici
        }, function(error, response, body) {
            let bici = JSON.parse(body).bicicleta
            expect(response.statusCode).toBe(200)
            expect(bici.color).toBe("rojo")
            expect(bici.modelo).toBe("urbana")
            done()
        })
      })
    })
  })

})
>>>>>>> master
