var mongoose = require('mongoose');
const Bicicleta = require('../../models/bicicleta');


describe('testing bicicletas',function(){
    
  beforeEach(function(done){
      setTimeout(function() {
        //Connection to the Database (Test Database)
          var mongoDB = 'mongodb://localhost/testdb'
          mongoose.connect(mongoDB, { useNewUrlParser: true})
          const db = mongoose.connection;
          db.on('error', console.error.bind(console, 'connection error'));
          db.once('open', function(){
          console.log('We are connected to test database!');
          });
          done();// es par terminar el beforeEach de otra manera no terminaria el metodo
        }, 100);
  });

  afterEach(function(done){
      Bicicleta.deleteMany({}, function(err, success){
        mongoose.disconnect(err); 
        done(); 
        if (err) console.log(err);
      });
  });

  describe('Bicicleta.allBicis', () => {
    it('comienza vacia', (done)  => {
      Bicicleta.allBicis(function(err, bicis){
        expect(bicis.length).toBe(0);
        done();
      });
    });
  });

  describe('Bicicleta.createInstance', () => {
    it('crea una instancia de Bicicleta', (done) => {
      var aBici = new Bicicleta({codigo:1,color:"verde", modelo:"urbana",ubicacion: [-34.5, -54.1]});
      Bicicleta.add(aBici, function(err, newBici){
        if ( err ) console.log(err);
        Bicicleta.allBicis(function(err, bicis){
          expect(bicis.length).toEqual(1);
          expect(bicis[0].color).toEqual(aBici.color);
          expect(bicis[0].modelo).toEqual(aBici.modelo);
          done(); //is used to solve problems of asynchronism
        });
      });
    });
  });



  describe('Bicicleta.add', () => {
    it('agrega solo una bici', (done) => {
      var aBici = new Bicicleta({codigo:1,color:"verde", modelo:"urbana",ubicacion: [-34.5, -54.1]});
      Bicicleta.add(aBici, function(err, newBici){
        if ( err ) console.log(err);
        //Here we are inside the callback , the adding(bicicleta) has just happend
        Bicicleta.allBicis(function(err, bicis){
          expect(bicis.length).toEqual(1);//make sure taht we add a bicicleta
          expect(bicis[0]._id).toEqual(aBici._id);//make sure was added the bicicleta correctly
          done(); //is used to solve problems of asynchronism
        });
      });
    });    
  });

  describe('Bicicleta.findByCode', () => {
    it('debe de devolver la bici con code x', (done) => {
      Bicicleta.allBicis(function(err, bicis){
        expect(bicis.length).toBe(0); 

        var aBici = new Bicicleta({codigo:1,color:"verde", modelo:"urbana",ubicacion: [-34.5, -54.1]});
        Bicicleta.add(aBici, function(err, newBici){
        if (err) console.log(err);
        
        Bicicleta.allBicis(function(err, bicis){
          aBici.codigo = bicis[0]._id;         
        });

        var aBici2 = new Bicicleta({codigo:2,color:"roja", modelo:"fixie",ubicacion: [-34.51, -54.12]});
        Bicicleta.add(aBici2, function(err, newBici){
          if (err) console.log(err);

          Bicicleta.findByCode(aBici.codigo, function(error, targetBici){
       
            expect(targetBici._id).toEqual(aBici.codigo);
            expect(targetBici.color).toBe(aBici.color);
            expect(targetBici.modelo).toBe(aBici.modelo);
            done();
          });
        });
        });
      });      
    });    
  });

});
