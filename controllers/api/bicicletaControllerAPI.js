var Bicicleta = require('../../models/bicicleta');

exports.bicicleta_list = function(req,res){
    Bicicleta.allBicis(function(err,bicis){
        res.status(200).json({
            Bicicletas:bicis
        });
    });
};

exports.bicicleta_create = function(req, res){
    const bici = new Bicicleta();
    bici.color = req.body.color;
    bici.modelo = req.body.modelo;
    bici.ubicacion = [req.body.lat, req.body.lng];
    Bicicleta.add(bici);
    res.status(200).json({bicicleta: bici});
};

exports.bicicleta_update = (req, res) => {
    const bici={
        color: req.body.color,
        modelo: req.body.modelo,
        ubicacion: [req.body.lat, req.body.lng]
    };

    Bicicleta.findByIdAndUpdate(bici._id, bici, function(err, update) {
        if (err){ res.status(500) }
        else{
            res.status(200).json({
                bici: bici
            });
        };
    });
};

exports.bicicleta_delete = (req, res) => {
    Bicicleta.removeByCode(req.body._id,  (err, deletedBici) => {
        if (err){ res.status(500) }
        else{ res.status(204).send(); }
    });
    
};