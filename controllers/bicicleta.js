var Bicicleta =require('../models/bicicleta');


exports.bicicleta_list =function(req, res){
    //All the bicicletas will show 
    Bicicleta.allBicis(function(err,bicis){
        res.render('bicicletas/index',{bicis:bicis});
    });
    
};



exports.bicicleta_create_get = function (req, res) {
    res.render('bicicletas/create');
}


exports.bicicleta_create_post =function(req, res){
    var bici = new Bicicleta({
        color: req.body.color,
        modelo: req.body.modelo,
        ubicacion: [req.body.lat, req.body.lng]
    })

    Bicicleta.add(bici, function (err, newBici) {
        console.log(newBici);
        res.redirect('/bicicletas');
    })
};


exports.bicicleta_update_get = function (req, res) {
    Bicicleta.findByCode(req.params._id, function (err, bici) {
        res.render('bicicletas/update', { bici });
    });
};

exports.bicicleta_update_post = function (req, res) {
    Bicicleta.findByCode(req.params._id, function (err, bici) {
        bici.color = req.body.color;
        bici.modelo = req.body.modelo;
        bici.ubicacion = [req.body.lat, req.body.lng];
        bici.save();
        
        res.redirect('/bicicletas');
    });
};

exports.bicicleta_delete_post = function (req, res) {
    Bicicleta.removeByCode(req.body._id, function (err) {
        res.redirect('/bicicletas');
    });
};

