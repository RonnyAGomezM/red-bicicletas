'use strict'

const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const bicicletaSchema = new Schema({
  color: String,
  modelo: String,
  ubicacion: {
    type: [Number], index: { type: '2dsphere', sparse: true }
  }
});

bicicletaSchema.statics.createInstance = function (_id,color,modelo,ubicacion){
    return new this({
        _id: _id,
        color: color,
        modelo: modelo,
        ubicacion: ubicacion
    });
}; 

bicicletaSchema.methods.toString = function (){
    return `code: ${this._id}  | color: ${this.color}`
};

bicicletaSchema.statics.add = function(aBici, cb){
    this.create(aBici, cb);
};

bicicletaSchema.statics.allBicis = function (cb){
    return this.find({},cb);
};

bicicletaSchema.statics.findByCode = function (aCode,cb){
    return this.findOne({_id:aCode},cb);
};

bicicletaSchema.statics.removeByCode = function (aCode,cb){
    console.log('imprimo codigo que se quiere eliminar');
    console.log(aCode);
    return this.deleteOne({_id:aCode},cb);
};

module.exports = mongoose.model('Bicicleta',bicicletaSchema);
